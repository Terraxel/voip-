# Liens utiles

## Repo Gitlab

- [Repo Gitlab](https://gitlab.com/Terraxel/voip-)

## Comparaison des différentes solutions

- [Top 10 des solutions open-source](http://wiki.ezuce.com/display/sipXcom/Quick+Start#QuickStart-DNSRecordsforYourDomain)

## SIPXCom

- [Explication SIPXCom](https://www.voip-info.org/sipx-solution-summary/)

- [Installation SIPXCom](http://wiki.ezuce.com/display/sipXcom/Quick+Start#QuickStart-DNSRecordsforYourDomain)

## Panasonic KX-NS1000

### Documentation

- [Doc KX-NS1000](https://www.teledynamics.com/tdresources/7ce1e2ee-a304-4892-874d-61e253f89ee0.pdf)

## Linksys SPA949

- [Doc SPA949](https://docplayer.fr/2983748-Telephone-ip-spa-guide-de-l-utilisateur-spa921-spa922-spa941-spa942-modele-telephonie.html)
